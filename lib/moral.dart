import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';
import 'home.dart';

class Moral extends StatefulWidget {


  Moral({Key key, @required this.moral}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String moral;

  @override
  _MoralState createState() => _MoralState();
}

class _MoralState extends State<Moral> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Center(
        child: Container(
          color: Theme
              .of(context)
              .accentColor,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                    child: Container(
                        padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 10.0),
                        child: Text(
                          'MORAL',
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
                          textAlign: TextAlign.center,
                        )
                    )
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    child: Text(
                      widget.moral,
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline6,
                      textAlign: TextAlign.justify,
                    )
                ),
                Container(
                    padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: Theme
                            .of(context)
                            .colorScheme
                            .copyWith(secondary: Colors.lightBlue[800]),
                      ),
                      child: FloatingActionButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Home(),
                              )
                          );
                        },
                        tooltip: 'next',
                        child: Text('Home'),
                      ),
                    )
                )
              ]
          ),
        ),
      ),
    );
  }
}