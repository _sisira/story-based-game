import 'package:flutter/material.dart';
import 'package:flutterapp/home.dart';
import 'package:flutterapp/main.dart';
import 'package:splashscreen/splashscreen.dart';

class Splash extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  @override
  Loadscreen createState() => Loadscreen();
}

class Loadscreen extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return SplashScreen(
      seconds: 3,
      gradientBackground: LinearGradient(
        // Where the linear gradient begins and ends
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        // Add one stop for each color. Stops should increase from 0 to 1
        stops: [0.1, 0.5, 0.7, 0.9],
        colors: [
          // Colors are easy thanks to Flutter's Colors class.
          Colors.indigo [800],
          Colors.indigo[600],
          Colors.indigo[400],
          Colors.indigo[200],
        ],
      ),
      image: Image.asset('assets/logo.png'),
      photoSize: 170,
      loadingText : Text('SEL GAMES FOR YOU'),
      navigateAfterSeconds: Home(),
    );
  }
}