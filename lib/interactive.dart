import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';

//void main() => runApp(MaterialApp(
//  home:Interactive(),
//));


class Interactive extends StatefulWidget{

  final dynamic scene;

  Interactive({Key key, @required this.title, @required this.scene}) : super(key: key);

  final String title;


  @override
  _InteractiveState createState() => _InteractiveState();
}

class _InteractiveState extends State<Interactive> {
  @override
  Widget build(BuildContext context) {
    var optionsList = [for(var i=0; i<widget.scene['options'].length; i+=1) i];
    return Scaffold(
//        appBar: AppBar(
//          title: Text(
//              'My Demo App',
//              style: TextStyle(
//                color: Colors.black,
//                fontWeight: FontWeight.bold,
//              )
//          ),
//          centerTitle: true,
//          backgroundColor: Colors.amber,
//        ),
        backgroundColor: Colors.grey[500],
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.fromLTRB(10, 60, 10, 10),
                        child : Image(
                          image: NetworkImage(widget.scene['image']),
                        )
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(
                      widget.scene['text'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Sri',
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: optionsList.map((i) => new SizedBox(
                          width:300.0,
                          child:FlatButton(
                            onPressed: (){
                              return showAlertDialog(context, widget.scene['options'][i]['iscorrect']);
                            },
                            color: Colors.white70,
                            child:Text(
                              widget.scene['options'][i]['desc'].toString(),
                            ),
                          ),
                        ),
                      ).toList()
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top:10.0,bottom: 30.0),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.lightBlue[800]),
                        ),
                        child: FloatingActionButton(
                          onPressed: (){
                            Navigator.pop(
                            context,
                            );
                          },
                          tooltip: 'next',
                          child: Icon(Icons.arrow_right),
                        ),
                      )
                  ),
                ]
            )
        )
    );
  }
}

showAlertDialog(BuildContext context, int isC) {
  Widget okButton = FlatButton(
    child: Text('OK'),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  String msg;
  String alertTitle;

  if(isC == 1 ) {
    msg = 'Great! You made the right call!';
    alertTitle = 'Way to Go!';
//    cnt = 1;
  }
  else{
    msg = 'That is not the best way out. Try Again!';
    alertTitle = 'Uh Oh!';
  }
  AlertDialog alert = AlertDialog(
    title: Text(alertTitle),
    content: Text(msg),
    actions: [
      okButton,
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context){
      return alert;
    },
  );

}

