import 'package:flutter/material.dart';
import 'storyUpdate.dart';

class storyHome extends StatefulWidget {
  @override
  _storyHomeState createState() => _storyHomeState();
}

class _storyHomeState extends State<storyHome> {

  Update story;
  int s;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    story = new Update();
    s = 0;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Demo'),
      ),
      body: Center(
        child: _getWidget(),
      ),
    );
  }

  Widget _getWidget(){
    if(s == 0){
      return RaisedButton(
        child: Text('Start Game'),
        onPressed: (){
          setState(() {
            s = 1;
          });
        },
      );
    }
    else{
      return Text(
        story.jsonData['title'][0],
      );
    }
  }

}
