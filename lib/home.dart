import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';
import 'package:flutterapp/story.dart';

class Home extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  @override
  Homescreen createState() => Homescreen();
}

class Homescreen extends State<Home> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        child: Container(
          color: Theme.of(context).accentColor,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Container(
                        padding: EdgeInsets.fromLTRB(20.0,60.0,20.0,0.0),
                        child: Image(
                          image: AssetImage('assets/gdwLogo.png'),
                          height: 270,
                        ),
                    ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 20),
                  child: RaisedButton(
                      onPressed : (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Home(),
                            )
                        );
                      },
                      child: Text('Guess The Word',
                        style: Theme.of(context).textTheme.headline5,
                        textAlign: TextAlign.justify,
                      )
                    ),
                ),
                Expanded(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(30.0,20.0,20.0,0.0),
                      child: Image(
                        image: AssetImage('assets/storyLogo.png'),
                        height: 270,
                      ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 50),
                  child: RaisedButton(
                      onPressed : (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Story(),
                            )
                        );
                      },
                      child: Text('Story Based Game',
                        style: Theme.of(context).textTheme.headline5,
                        textAlign: TextAlign.justify,
                      )
                  ),
                ),
              ]
          ),
        ),
      ),
    );
  }
}