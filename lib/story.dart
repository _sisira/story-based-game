import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';
import 'package:flutterapp/nonInteractive.dart';
import 'storyUpdate.dart';
import 'playStory.dart';

class Story extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  @override
  StoryList createState() => StoryList();
}

class StoryList extends State<Story> {

  Update story;
  int s;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    story = new Update();
    s=0;
  }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
            child: _getWidget(),
            ),
          //Center(
//        child: Container(
//          color: Theme.of(context).accentColor,
//          child: Column(
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//              crossAxisAlignment: CrossAxisAlignment.stretch,
//              children://<Widget>[
//                Expanded(
//                  child: Container(
//                    padding: EdgeInsets.fromLTRB(20.0,60.0,20.0,0.0),
//                    child: Image(
//                      image: AssetImage('assets/img.png'),
//                      height: 270,
//                    ),
//                  ),
//                ),
//                Container(
//                  padding: EdgeInsets.only(bottom: 20),
//                  child: RaisedButton(
//                      onPressed : (){
//                        Navigator.pushNamed(
//                          context, '/noninteractive',
//                        );
//                      },
//                      child: Text('Rahul and his Dilemma',
//                        style: Theme.of(context).textTheme.headline5,
//                        textAlign: TextAlign.justify,
//                      )
//                  ),
//                ),
//                Expanded(
//                  child: Container(
//                    padding: EdgeInsets.fromLTRB(30.0,20.0,20.0,0.0),
//                    child: Image(
//                      image: AssetImage('assets/storyLogo.png'),
//                      height: 270,
//                    ),
//                  ),
//                ),
//                Container(
//                  padding: EdgeInsets.only(bottom: 50),
//                  child: RaisedButton(
//                      onPressed : (){
//                        Navigator.pushNamed(
//                          context, '/story',
//                        );
//                      },
//                      child: Text('Story 2',
//                        style: Theme.of(context).textTheme.headline5,
//                        textAlign: TextAlign.justify,
//                      )
//                  ),
//                ),
              //]
//          ),
//        ),
//      ),
    );
  }

  Widget _getWidget(){
    if(s == 0){
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children : <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 30, 20, 40),
//                decoration: BoxDecoration(
//                  border: Border.all(width: 5),
//                  borderRadius: BorderRadius.circular(12),
//                ),
              child: Text("Did you know that we humans use our brains, not only to solve math and science equations, but also to make  decisions and understand one's emotions? Here's a game for you to know how well versed you are with the latter!",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20,),
                textAlign: TextAlign.justify,
              ),
            ),
            RaisedButton(
              child: Text("Let's Go"),
              onPressed:(){
                setState(() {
                  s = 1;
                });
              },
            ),
          ]
      );
    }
    else{
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: story.jsonData['title'].map<Widget>((title){
          return RaisedButton(
              onPressed:(){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PlayStory(title: title, scenes:story.jsonData['data'][title]['scenes'], moral:story.jsonData['data'][title]['moral']),
                )
                );
              },
              child: Text(title.toString()),
              color:Colors.cyan,
          );
        }).toList(),
      );
    }
  }
}