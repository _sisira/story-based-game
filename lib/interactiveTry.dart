import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';
import 'moral.dart';
import 'nonInteractiveTry.dart';

//void main() => runApp(MaterialApp(
//  home:Interactive(),
//));


class InteractiveTry extends StatefulWidget{

  final dynamic scene;
  final List<dynamic> scenes;
  final int i;
  final int n;


  InteractiveTry({Key key, @required this.title, @required this.scene, @required this.i, @required this.n, @required this.scenes, @required this.moral}) : super(key: key);

  final String moral;
  final String title;


  @override
  _InteractiveStateTry createState() => _InteractiveStateTry();
}

class _InteractiveStateTry extends State<InteractiveTry> {
  @override
  Widget build(BuildContext context) {
    var optionsList = [for(var i=0; i<widget.scene['options'].length; i+=1) i];
    return Scaffold(
//        appBar: AppBar(
//          title: Text(
//              'My Demo App',
//              style: TextStyle(
//                color: Colors.black,
//                fontWeight: FontWeight.bold,
//              )
//          ),
//          centerTitle: true,
//          backgroundColor: Colors.amber,
//        ),
        backgroundColor: Colors.grey[500],
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.fromLTRB(10, 60, 10, 10),
                        child : Image(
                          image: NetworkImage(widget.scene['image']),
                        )
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(
                      widget.scene['text'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Sri',
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: optionsList.map((i) => new SizedBox(
                          width:300.0,
                          child:FlatButton(
                            onPressed: (){
                              return showAlertDialog(context, widget.scene['options'][i]['iscorrect']);
                            },
                            color: Colors.white70,
                            child:Text(
                              widget.scene['options'][i]['desc'].toString(),
                            ),
                          ),
                        ),
                        ).toList()
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top:10.0,bottom: 30.0),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.lightBlue[800]),
                        ),
                        child: FloatingActionButton(
                          onPressed: (){
                            if(widget.i != widget.n) {
                              var i = widget.i +1;
                              if (widget.scenes[i]['interactive'] == 1) {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          InteractiveTry(title: widget.title,
                                              scene: widget.scenes[i],
                                              i : i,
                                              n : widget.n,
                                              scenes: widget.scenes,
                                              moral : widget.moral,
                                          )
                                    )
                                );
                              }
                              else{
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            NonInteractiveTry(title: widget.title,
                                              scene: widget.scenes[i],
                                              i : i,
                                              n : widget.n,
                                              scenes: widget.scenes,
                                              moral : widget.moral,
                                            )
                                    )
                                );
                              }
                            }
                            else{
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        Moral(moral: widget.moral),
                                  )
                              );
                            }
                          },
                          tooltip: 'next',
                          child: Icon(Icons.arrow_right),
                        ),
                      )
                  ),
                ]
            )
        )
    );
  }
}

showAlertDialog(BuildContext context, int isC) {
  Widget okButton = FlatButton(
    child: Text('OK'),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  String msg;
  String alertTitle;

  if(isC == 1 ) {
    msg = 'Great! You made the right call!';
    alertTitle = 'Way to Go!';
//    cnt = 1;
  }
  else{
    msg = 'That is not the best way out. Try Again!';
    alertTitle = 'Uh Oh!';
  }
  AlertDialog alert = AlertDialog(
    title: Text(alertTitle),
    content: Text(msg),
    actions: [
      okButton,
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context){
      return alert;
    },
  );

}

