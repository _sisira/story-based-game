import 'package:flutter/material.dart';
import 'package:flutterapp/interactive.dart';
import 'package:flutterapp/nonInteractive.dart';
import 'package:flutterapp/splash.dart';
import 'package:flutterapp/home.dart';
import 'package:flutterapp/story.dart';
import 'storyHome.dart';
import 'storyUpdate.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    final appName = 'The Sad Life Of Rahul';
    return MaterialApp(
      title: appName,
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan[100],

        // Define the default font family.
        fontFamily: 'Sri',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline5: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'New', color: Colors.black),
          headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Sri', color: Colors.black),
        ),
      ),
      home: Splash(),
    );
  }
}





