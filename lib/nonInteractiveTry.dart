import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';
import 'moral.dart';
import 'interactiveTry.dart';

class NonInteractiveTry extends StatefulWidget {

  final dynamic scene;
  final List<dynamic> scenes;
  final int i;
  final int n;

  NonInteractiveTry({Key key, @required this.title, @required this.scene, @required this.i, @required this.n, @required this.scenes, @required this.moral}) : super(key: key);

  final String moral;
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _NonInteractiveStateTry createState() => _NonInteractiveStateTry();
}

class _NonInteractiveStateTry extends State<NonInteractiveTry> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Center(
        child: Container(
          color: Theme.of(context).accentColor,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                    child: Container(
                        padding: EdgeInsets.only(top: 30.0, bottom: 10.0),
                        child: Image(
                          image: NetworkImage(widget.scene['image']),
                        )
                    )
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(20.0,10.0,20.0,10.0),
                    child: Text(
                      widget.scene['text'],
                      style: Theme.of(context).textTheme.headline6,
                      textAlign: TextAlign.justify,
                    )
                ),
                Container(
                    padding: EdgeInsets.only(top:10.0,bottom: 30.0),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.lightBlue[800]),
                      ),
                      child: FloatingActionButton(
                        onPressed: (){
                          if(widget.i != widget.n) {
                            var i = widget.i +1;
                            if (widget.scenes[i]['interactive'] == 1) {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          InteractiveTry(title: widget.title,
                                            scene: widget.scenes[i],
                                            i : i,
                                            n : widget.n,
                                            scenes: widget.scenes,
                                            moral : widget.moral,
                                          )
                                  )
                              );
                            }
                            else{
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          NonInteractiveTry(title: widget.title,
                                            scene: widget.scenes[i],
                                            i : i,
                                            n : widget.n,
                                            scenes: widget.scenes,
                                            moral : widget.moral,
                                          )
                                  )
                              );
                            }
                          }
                          else{
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Moral(moral: widget.moral),
                                )
                            );
                          }
                        },
                        tooltip: 'next',
                        child: Icon(Icons.arrow_right),
                      ),
                    )
                )
              ]
          ),
        ),
      ),
    );
  }
//  int n = 8;
//  List<String> _imgList = ['assets/img.png','assets/img2.png','assets/img3.png','assets/img4.png','assets/img5.png','assets/img6.png','assets/img7.png','assets/img8.png'];
//  List<String> _textList = ['This is Rahul. He is studying 8th grade. He is a shy boy who loves cricket. He struggles to cope with academics; specially mathematics.','Page2','Page3','Page4','Page5','Page6','Page7','Page8'];
//  String _counter = 'This is Rahul. He is studying 8th grade. He is a shy boy who loves cricket. He struggles to cope with academics; specially mathematics.';
//  String _imgLoc = 'assets/img.png';
//
//  int cnt = 0;
//  void _incrementCounter() {
//    if(cnt==1 || cnt==3 || cnt==5) {
//      Navigator.pushNamed(
//          context, '/interactive');
//      cnt++;
//    }
//      setState(() {
//        // This call to setState tells the Flutter framework that something has
//        // changed in this State, which causes it to rerun the build method below
//        // so that the display can reflect the updated values. If we changed
//        // _counter without calling setState(), then the build method would not be
//        // called again, and so nothing would appear to happen.
//
//        if(cnt < (n-1)) {
//          cnt++;
//          _counter = _textList[cnt];
//          _imgLoc = _imgList[cnt];
//        }
//      });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    // This method is rerun every time setState is called, for instance as done
//    // by the _incrementCounter method above.
//    //
//    // The Flutter framework has been optimized to make rerunning build methods
//    // fast, so that you can just rebuild anything that needs updating rather
//    // than having to individually change instances of widgets.
//    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//      ),
//      body: Center(
//        child: Container(
//          color: Theme.of(context).accentColor,
//          child: Column(
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//              crossAxisAlignment: CrossAxisAlignment.stretch,
//              children: <Widget>[
//                Expanded(
//                    child: Container(
//                        padding: EdgeInsets.only(top: 30.0, bottom: 10.0),
//                        child: Image(
//                          image: AssetImage('$_imgLoc'),
//                        )
//                    )
//                ),
//                Container(
//                    padding: EdgeInsets.fromLTRB(20.0,10.0,20.0,10.0),
//                    child: Text(
//                      '$_counter',
//                      style: Theme.of(context).textTheme.headline6,
//                      textAlign: TextAlign.justify,
//                    )
//                ),
//                Container(
//                    padding: EdgeInsets.only(top:10.0,bottom: 30.0),
//                    child: Theme(
//                      data: Theme.of(context).copyWith(
//                        colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.lightBlue[800]),
//                      ),
//                      child: FloatingActionButton(
//                        onPressed: (){
//                          _incrementCounter();
//                        },
//                        tooltip: 'next',
//                        child: Icon(Icons.arrow_right),
//                      ),
//                    )
//                )
//              ]
//          ),
//        ),
//      ),
//    );
//}
//  void display(){
//    for(var i=0; i<widget.scenes.length; i++){
//      print(widget.scenes[i]['text']);
//    }
//  }
}