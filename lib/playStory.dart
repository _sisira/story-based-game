import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/main.dart';
import 'package:flutterapp/nonInteractive.dart';
import 'interactive.dart';
import 'moral.dart';
import 'interactiveTry.dart';
import 'nonInteractiveTry.dart';

class PlayStory extends StatefulWidget {

  final List<dynamic> scenes;



  PlayStory({Key key, @required this.title, @required this.scenes, @required this.moral}) : super(key: key);

  final String moral;

  final String title;
  @override
  _State createState() => _State();
}

class _State extends State<PlayStory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Ready to Play?"),
        backgroundColor: Colors.cyan,
      ),
      body: Center(
        child : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children : <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(20, 30, 20, 40),
//                decoration: BoxDecoration(
//                  border: Border.all(width: 5),
//                  borderRadius: BorderRadius.circular(12),
//                ),
                child: Text("Take the right decisions and make it to end of the story "+ widget.title+"",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20,),
                  textAlign: TextAlign.justify,
                ),
              ),
              RaisedButton(
                onPressed: start,
                child: Text("Play Game"),
                color: Colors.amber,
              ),
            ]
        ),
      )
    );
  }

  void start(){

    var i = 0;
    var n = widget.scenes.length - 1;


    if(widget.scenes[i]['interactive'] == 1) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>
                InteractiveTry(title: widget.title, scene: widget.scenes[i], i: i, n: n, scenes: widget.scenes, moral: widget.moral),
          )
      );
    }
    else{
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>
                NonInteractiveTry(title: widget.title, scene: widget.scenes[i], i: i, n: n, scenes: widget.scenes, moral: widget.moral),
          )
      );
    }

//    Navigator.push(
//        context,
//        MaterialPageRoute(
//          builder: (context) => Moral(moral: widget.moral),
//        )
//    );
//    for(var i=widget.scenes.length-1; i>=0; i--){
//      if(widget.scenes[i]['interactive'] == 1){
//        print(widget.scenes[i]);
//        Navigator.push(
//          context,
//          MaterialPageRoute(
//            builder: (context) => Interactive(title: widget.title, scene: widget.scenes[i]),
//          )
//        );
//      }else{
//        print(widget.scenes[i]);
//        Navigator.push(
//          context,
//          MaterialPageRoute(
//            builder: (context) => NonInteractive(title: widget.title, scene: widget.scenes[i]),
//          )
//        );
//      }
//    }


  }
}
